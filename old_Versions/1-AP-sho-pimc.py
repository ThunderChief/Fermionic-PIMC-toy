'''
A path-integral quantum Monte Carlo program to compute the energy of two 
fermions in a simple harmonic oscillator potential in one spatial
dimension with open boundary conditions.
'''
'''
A note: this implementation is lazy and just assumes two or more electrons are 
in a box that is separated by a simple barrier.

It could easily be improved upon to be something more than it is, but this is 
just a toy for testing.
'''
import numpy as np
import math
# ------------------------------------------------------------------------------
def SHOEnergyExact(T):
    ''' The exact SHO energy for \hbar \omega / k_B = 1 '''
    return 0.5/np.tanh(0.5/T)

# ------------------------------------------------------------------------------
def HarmonicOscillator(R):
    ''' SHO potential with m = 1 and \omega = 1 '''
    return 0.5*np.dot(R,R)

# ------------------------------------------------------------------------------
'''
def InteractionPotential(alpha=1,R1,R2):
#    The interaction potential between the electrons. Charge set =1
    return alpha*alpha/np.abs(R1-R2)
    # InteractionPotential(alpha=1,Path.bead
    # Don't use this for the moment, get the rest of it working
'''
# ------------------------------------------------------------------------------
def modTslice(tslice,numSlices):
    return tslice % numSlices

# ------------------------------------------------------------------------------
def cutOff(beadA, beadB,cutoff=12):
    if (beadA - beadB ) < cutoff:
        return True
    else:
        return False

# ------------------------------------------------------------------------------
def determinant(Path,tslice):
    '''In the Lyubartsev paper, J (Lyu.) == M (Prok.), so it corresponds to the 
    number of beads on a line.'''
# This is where an optimized determinant algorithm would go
    expArray = np.zeros((Path.numParticles,Path.numParticles)) 
    # Create array with the dimensions of N x N
    tMod = modTslice(tslice,Path.numTimeSlices)
    tModPlus = modTslice(tslice + 1,Path.numTimeSlices)
    setZero = False

    for ptclRow in range(Path.numParticles):
        for ptclCol in range(Path.numParticles):
            # Iterate over the beads. 
            # Why read from Path.beads? expArray is a copy anyways
            setZero = cutOff(Path.beads[tMod,ptclRow], Path.beads[tMod,ptclCol])

            if setZero:
                expArray[ptclRow,ptclCol] = 0
            else:
                expArray[ptclRow,ptclCol] = np.exp(Path.numTimeSlices/(-2*Path.tau) *
                    (Path.beads[tMod,ptclRow] - Path.beads[tModPlus,ptclCol])**2)
    
    return np.linalg.det(expArray)
    # return det(expArray)

# ------------------------------------------------------------------------------
def instantiateOriginalPotentials(Path, potentialArray):
    for ptcl in range(Path.numParticles):
        for tslice in range(Path.numTimeSlices):
            tMod = modTslice(tslice,Path.numTimeSlices)
            tModPlus = modTslice(tslice + 1,Path.numTimeSlices)
            potentialArray[tslice,ptcl] = 1/ np.math.factorial(Path.numParticles)*np.exp( Path.numTimeSlices/(-2*Path.tau)*
            (Path.Vext(Path.beads[tMod,ptcl]) + Path.Vext(Path.beads[tModPlus,ptcl])))

# ------------------------------------------------------------------------------
def updatePotentials(Path,potentialArray,tslice,ptcl):
    tMod = modTslice(tslice,Path.numTimeSlices)
    tModPlus = modTslice(tslice+1,Path.numTimeSlices)
    setZero = cutOff(Path.beads[tMod,ptcl], Path.beads[tModPlus,ptcl])

    if setZero:
        potentialArray[tslice,ptcl] = 0
    else:
        potentialArray[tslice,ptcl] = 1/np.math.factorial(Path.numParticles) * np.exp(Path.numTimeSlices/(-2*Path.tau) *
            (Path.Vext(Path.beads[tMod,ptcl]) + Path.Vext(Path.beads[tModPlus,ptcl])))
        
# ------------------------------------------------------------------------------
class Paths:
    ''' The set of worldliens, action, and estimators. '''
    def __init__(self,beads,tau,lam):
        self.tau = tau  # Equal to 1/(M*T) = beta/M
        self.lam = lam
        self.beads = np.copy(beads) # This is M (Prok.) == J (Lyu.)
        self.numTimeSlices = len(beads)
        self.numParticles = len(beads[0])

    def SetPotential(self,externalPotentialFunction):
        ''' The potential function. This is a lot easier than in C++ '''
        self.VextHelper = externalPotentialFunction

    def Vext(self,R):
        ''' The external potential energy. '''
        return self.VextHelper(R)
    ''' This is where the V_ext magic happens - Vext will need to call 
    SetPotential and pass the variables on to it for it to do the work '''

    def Vint(self,alpha,R1,R2):# THIS NEEDS REEVALUATION
        ''' The interaction potential energy. '''
        return self.VintHelper(alpha,R1,R2)
        # In this case, R_i is beads[tslice_i,ptcl]

    def PotentialAction(self,tslice):
        ''' The potential action '''
        pot = 0.0
        alpha = 1
        for ptcl in range(self.numParticles):
            pot += self.Vext(self.beads[tslice,ptcl])
#            pot += self.Vint(alpha,self.beads[tslice,ptcl]) # Vint(alpha,R1,R2)
            # Compute exchange and self-interaction?
        return self.tau*pot
    ''' Iterate over the number of particles. Add to pot the potential the 
    specified particle feels. Multiply by time slice.'''

    def KineticEnergy(self):
        '''The thermodynamic kinetic energy estimator.'''
        tot = 0.0
        norm = 1.0/(4.0 * self.lam * self.tau * self.tau)
        for tslice in range(self.numTimeSlices):
            tslicep1 = (tslice + 1) % self.numTimeSlices
            for ptcl in range(self.numParticles):
                delR = self.beads[tslicep1,ptcl] - self.beads[tslice,ptcl]
                tot = tot - norm * np.dot(delR,delR)

        KE = 0.5 * self.numParticles/self.tau + tot/(self.numTimeSlices)
        return KE
    # self.tau = time step, self.lam = mass
    # For each timeslice, determine the next timeslice. Then, for each particle,
    # determine the change in R (delR) by comparing the positions of the beads.
    # Thermodynaic energy estimator is the "collective KE energy" - it only care
    # about the difference of the system as a whole, not any individual part.
    # tot is "total dispalcement" of the collective system. np.dot(delR,delR) is
    # delR^2, so delR/tau = v. KE is the per-particle-averaged kinetic energy

    def PotentialEnergy(self):
        '''The operator potential energy estimator.'''
        PE = 0.0
        for tslice in range(self.numTimeSlices):
            for ptcl in range(self.numParticles):
                R = self.beads[tslice,ptcl]
                PE = PE + self.Vext(R)
        return PE/(self.numTimeSlices)
    # For each time step, for each particle at each time step, where R is the 
    # particular bead's current position (initially a random value), sum over
    # the potential energy that that particular bead feels due to the potential.
    # Add that value to the running total, then iterate over beads in time.
    
    '''def InteractionEnergy(self):'''

    def Energy(self):
        '''The total energy.'''
        return self.PotentialEnergy() + self.KineticEnergy()

def PIMC(numSteps,Path):
    '''Perform a path integral Monte Carlo simulation of length numSteps.'''
    observableSkip = 50
    equilSkip = 1000
    numAccept = {'CenterOfMass':0,'Staging':0}  # Used for CoM/Staging/Worm

    EnergyTrace = []   # Array holding the values of the trace of the density
                                    # matrix
    delta = 0.75           # I should ask what this is about

    # Set-up the Potential and Determinant arrays
    potentialArray = np.zeros([Path.numTimeSlices,Path.numParticles])
    determinantArray = np.zeros([Path.numTimeSlices,Path.numParticles])
    
    instantiateOriginalPotentials(Path,potentialArray)
    for tslice in range(Path.numTimeSlices):
        for ptcl in range(Path.numParticles):
            # Initialize the determinantArray - entries based on original random values
            determinantArray[tslice,ptcl] = determinant(Path,tslice)
    
    # iterate
    for steps in range(0,numSteps + equilSkip + 1):
        oldBead = np.zeros_like(Path.beads)
        oldAction = 0.0
        for ptcl in range(Path.numParticles):
            for time in range(Path.numTimeSlices):
                # "steps" needs to wrap for end points
                oldBead[time,ptcl] = Path.beads[time,ptcl]

                # Compute old action
                oldAction += Path.PotentialAction(time)

        # Roll new position
        Path.beads[time,ptcl] += delta*(-1.0 + 2.0*np.random.random())

        # Calculate new potential, determinants
        for ptcl in range(Path.numParticles):
            for time in range(Path.numTimeSlices):
                updatePotentials(Path,potentialArray,time,ptcl)
                determinantArray[time - 1,ptcl] = determinant(Path,time-1)
                determinantArray[time,ptcl] = determinant(Path,time)

        # Compute new action
        newAction = 0.0
        for ptcl in range(Path.numParticles):
            for time in range(Path.numTimeSlices):
                newAction += Path.PotentialAction(time)

        # Compare old action and new action
        accepted = 0.0
        # Accept or reject and restore bead position
        if np.random.random() < np.exp(-(newAction - oldAction)):
            accepted += 1
        else:
            Path.beads = oldBead 
        # Measure the energy
        if (steps % observableSkip == 0) and (steps > equilSkip):
            if(steps == 1050):
                print("Now recording energies...\n")
            print("Path.Energy() = %d" % Path.Energy())
            EnergyTrace.append(Path.Energy())
    
    # End of the program, basically
    print("Absolute # of accepted: %d" % accepted)
    print("Accepted: %6.4f" % (accepted / numSteps))
    return np.array(EnergyTrace)

# ------------------------------------------------------------------------------
def main():
    T = 1.00 # Temperature in Kelvin
    lam = 0.5 # \hbar^2/2m k_b

    numParticles = 2
    numTimeSlices = 20  # Determines number of beads on a world line
    numMCSteps = 100000
    tau = 1.0/(T*numTimeSlices) # Imaginary time
    binSize = 100       # Data binning

    print('Simulation Parameters:')
    print('N      = %d' % numParticles)
    print('tau    = %6.4f' % tau)
    print('lambda = %6.4f' % lam)
    print('T      = %4.2f\n' % T)
    print('numMCSteps = ', numMCSteps)
    # fix the random seed for repeatability
    np.random.seed(1173)

    # initliaze the main data structure
    beads = np.zeros([numTimeSlices,numParticles])

    # random initial positions (classical state)
    for tslice in range(numTimeSlices):
        for ptcl in range(numParticles):
            beads[tslice,ptcl] = 0.5 * (-1 + 2.0 * np.random.random())
                # Wasn't random.random() not supposed to be used?

    # setup the paths
    Path = Paths(beads,tau,lam)
    Path.SetPotential(HarmonicOscillator)
    
    # Compute the energy via path-integral Monte Carlo
    Energy = PIMC(numMCSteps,Path)

    # Do some simple binning statistics
    print("len(Energy) = ",len(Energy))
    print("binSize = ", binSize)
    numBins = int(1.0*len(Energy)/binSize)
    print("numBins = ", numBins)
    slices = np.linspace(0, len(Energy),numBins+1,dtype=int)
    binnedEnergy = np.add.reduceat(Energy, slices[:-1]) / np.diff(slices)
            # REMINDER: look up what add.reduceat() and diff() do

    # Output the final result
    print('Energy = %8.4f +/- %6.4f' % (np.mean(binnedEnergy),
                                       (np.std(binnedEnergy)/np.sqrt(numBins-1))))
    print('Eexact = %8.4f' % SHOEnergyExact(T))

# ------------------------------------------------------------------------------
if __name__ == "__main__":
    main()
# What purpose does this serve? "Traditionally done"?
