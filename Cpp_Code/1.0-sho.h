struct Paths;

double ExtPotential(double R);
int ModTslice(int tSlice, int numSlices);
bool CutOff(double beadA, double beadB, double cutOff);
double PotentialAction(double beadVector[], double tau, int numParticles);
double KineticEnergy(Paths &Path, double tau);
double PotentialEnergy(Paths &Path);
double Energy(Paths &Path, double tau);
double Determinant(Paths &Path, int tSlice);
void InstantiatePotentials(Paths &Path, int iter);
double ComputeAction(Paths &Path, int tSlice, int iter);
void PIMC(Paths &Path, int numSteps, int iter, int numEquilStep, int observSkip);
