using LinearAlgebra # For Determinant()
using Printf        # I don't think I'm actually using this...

using PyCall

mutable struct Paths
    numParticles::Int64
    numTimeSlices::Int64
    beads::Array{Float64,}
    lam::Float64
    tau::Array{Float64}
    determinants::Array{Float64}
    potentials::Array{Float64}
    expPots::Array{Float64}
    energy::Array{Float64} # Format: [KE,PE]
    numAccepted::Int64
end

function ExtPotential(R::Float64)
    # The external potential to use - use theory units
    return 0.5 * R * R
end

# Inline this for performance
@inline function ModTslice(tslice::Int64,numSlices::Int64)
    # Computes the modular time slice to use
    # +1 because Julia is 1-indexed, not 0-indexed like Python or C
    return (( tslice - 1 ) % numSlices ) + 1
end

# Inline this for performance
@inline function cutOff(beadA::Float64,beadB::Float64,cutoff=10^-12)
    # Flags whether or not to truncate the calculation
    if abs(beadA - beadB) < cutoff
        return true
    else
        return false
    end
end

function PotentialAction(Path::Paths,tslice::Int64,iter::Int64)
    # Computes the potential action of the system
    # A different comparison will be used for the MC comparison
    pot = 0.0
    for ptcl = 1:Path.numParticles
        pot += ExtPotential(Path.beads[tslice,ptcl])
    end
    return Path.tau[iter] * pot
end

function KineticEnergy(Path::Paths,iter::Int64)
    # Computes the KE of the particle(s)
    #Path.energy[1] = 0.0
    tot = 0.0
    norm = 1.0/(4.0 * Path.lam * Path.tau[iter] * Path.tau[iter])
    for tslice = 1:Path.numTimeSlices
        tModPlus = ModTslice(tslice + 1, Path.numTimeSlices)
        for ptcl = 1:Path.numParticles
            delR = Path.beads[tModPlus,ptcl] - Path.beads[tslice,ptcl]
            
            tot = tot - norm * delR * delR
        end
    end
    return 0.5 * Path.numParticles / Path.tau[iter] + tot/Path.numTimeSlices
end

function PotentialEnergy(Path::Paths,iter::Int64)
    # Computes the potential energy of the particle(s)
    Path.energy[2] = 0.0
    for tslice = 1:Path.numTimeSlices
        for ptcl = 1:Path.numParticles
            R = Path.beads[tslice,ptcl]
            Path.energy[2] = Path.energy[2] + ExtPotential(R)
        end
    end

    return Path.energy[2]/Path.numTimeSlices
end

function Energy(Path::Paths,iter::Int64)
    # Returns the KE + PE of the particle(s)
    Path.energy[1] = KineticEnergy(Path,iter)
    Path.energy[2] = PotentialEnergy(Path,iter)
    return (Path.energy[1] + Path.energy[2])
end

function Determinant(Path::Paths,tslice::Int64,iter::Int64)
    # Just short-circuit the whole thing for Boltzmannons
    return 1
    tMod = ModTslice(tslice,Path.numTimeSlices)
    tModPlus = ModTslice(tslice + 1,Path.numTimeSlices)
    tau = Path.tau[iter]

    if (Path.numParticles == 1)
        return 1    # Comment out to return exponential thing
        return exp(-1/(2*tau)) * 
            (Path.beads[tMod,1] - Path.beads[tModPlus,1])^2 
    end

    for ptclRow = 1:Path.numParticles
        for ptclCol = 1:Path.numParticles
            # Iterage over the beads - recall, Julia is Column-major
            if (cutOff(Path.beads[tMod,ptclRow], Path.beads[tModPlus,ptclCol]))
                Path.determinants[ptclRow,ptclCol] = 0
            else
                Path.determinants[ptclRow,ptclCol] = exp(-1/(2*tau)) * 
                    (Path.beads[tMod,ptclRow] - Path.beads[tModPlus,ptclCol])^2
            end
        end
    end
    return det(Path.determinants)
end

function InstantiatePotentials(Path::Paths,iter::Int64)
    for tslice = 1:Path.numTimeSlices
        for ptcl = 1:Path.numParticles
            tMod = ModTslice(tslice,Path.numTimeSlices)
            tModPlus = ModTslice(tslice+1,Path.numTimeSlices)
            vextT = ExtPotential(Path.beads[tMod,ptcl])
            vextTPlus = ExtPotential(Path.beads[tModPlus,ptcl])

            if (cutOff(Path.beads[tMod,ptcl],Path.beads[tModPlus,ptcl]))
                Path.potentials[tslice,ptcl] = 0
            else
                Path.potentials[tslice,ptcl] = -1/2 * 
                (ExtPotential(Path.beads[tMod,ptcl]) + 
                                        ExtPotential(Path.beads[tModPlus,ptcl])) 
            end
        end
    end
end

# This should be something like Path.potential[]*Path.determinants[] for action
function ComputeAction(Path::Paths,tslice::Int64,iter::Int64)
    # Computes the potential action of a particle along its worldline
    action = 0.0
    tMod = ModTslice(tslice,Path.numTimeSlices)
    tModPlus = ModTslice(tslice + 1,Path.numTimeSlices)

    for ptcl = 1:Path.numParticles
        if (cutOff(Path.beads[tMod,ptcl],Path.beads[tModPlus,ptcl]))
            action += 0
        else
            action += 1/2 * 
                (ExtPotential(Path.beads[tMod,ptcl]) + 
                                        ExtPotential(Path.beads[tModPlus,ptcl]))
        end
    end

    return action
end


function PIMC(numSteps::Int64,Path::Paths,iter::Int64,numEquilibStep::Int64,
               observableSkip::Int64)
    #observableSkip = observableSkip
    equilSkip = numEquilibStep
    energyTrace = Float64[] # Array to store the calculated energies
    potentialTrace = Float64[]  # Stores potential energy values
    kineticTrace = Float64[]    # Stores kinetic energy values
    delta = 1.0 # 0.75
#    firstRun = true

    # Initialize determinants array
    for tslice = 1:Path.numTimeSlices
        for ptcl = 1:Path.numParticles
            Path.determinants[tslice,ptcl] = Determinant(Path,tslice,iter)
        end
    end

    # Initialize potentials array
    InstantiatePotentials(Path,iter)
    
#    println("\nComputing MC simulation...")
    for steps = 1:numSteps
        shift = delta * (-1.0 + 2.0 * rand())

        # MC iterations
        for y = 1:Path.numParticles
            # These are supposed to be random ints
            article = rand(1:Path.numParticles,1,1)
            Particle = article[1]
            for z = 1:Path.numTimeSlices
                oldBeads = Path.beads
                ime = rand(1:Path.numTimeSlices,1,1)
                Time = ime[1]

                oldAction = 0.0
                newAction = 0.0

                # Compute the old action
                for tslice = 1:Path.numTimeSlices
                    oldAction += ComputeAction(Path,tslice,iter)
                end
                Path.beads[Time,Particle] += shift

                # Compute the new action
                for tslice = 1:Path.numTimeSlices
                    newAction += ComputeAction(Path,tslice,iter)
                end

                # MC Accept/Reject results
                if rand() < exp(-Path.tau[iter]*(newAction - oldAction))
                    Path.numAccepted += 1
                else
                    Path.beads = oldBeads
                end
                
#                if (steps == (observableSkip + equilSkip) && 
#                           (firstRun == true))
#                    firstRun = false
#                    println("Now recording energies...\n")
#                end

                if (steps % observableSkip == 0) && (steps > equilSkip)
#                    println("Computed energy = ",Energy(Path,iter))
                    push!(energyTrace, Energy(Path,iter))
                    push!(kineticTrace,Path.energy[1])
                    push!(potentialTrace,Path.energy[2])
                end
#=
                open("julia_beads_positions.txt","a") do io
                    println(io, oldBeads)
                    close(io)
                end
=#                
            end
        end
    end

    return energyTrace, kineticTrace, potentialTrace
end

function PyBinData(energyTrace::Array{Float64},binSize::Int64,
                    numParticles::Int64,temp::Float64,equil::Int64,obs::Int64,
                    numTimeSlices::Int64)
    py"""
    import numpy as np
    #import mpmath as mp
    pyTemp = $temp
    pyEquil = $equil
    pyObs = $obs
    pyNumTimeSlices = $numTimeSlices
    pyNumParticles = $numParticles
    Energy = $vec($energyTrace)
    pyBinSize = $binSize
    numBins = int(1.0 * len(Energy)/pyBinSize)
    slices = np.linspace(0,len(Energy),numBins + 1,dtype=int)
    binnedEnergy = np.add.reduceat(Energy,slices[:-1]) / np.diff(slices)
    sumEnergy = np.mean(binnedEnergy)/11605
    errEnergy = np.std(binnedEnergy)/np.sqrt(numBins-1)/11605
    # 11605 K per 1 eV
    #print('Energy = %8.4f +/- %6.4f' % (sumEnergy, errEnergy))
    #print('Eexact = 1/2*coth(1/( 2T )) = ', $numParticles*1/4*mp.coth(1/(2 * $temp)))
    name = "data_T_" + str(pyTemp) + "-Eq_" + str(pyEquil) + "-Obs_" + str(pyObs) + "-nB_" + str(pyNumTimeSlices) + "-nP_" + str(pyNumParticles) + ".txt"
    
    with open(name,"a") as output:
        print(sumEnergy,file=output)
    """
end

# Optimize - https://docs.julialang.org/en/v1/manual/performance-tips/
function main()
    lam = 24.24 * 1836  # \hbar^2/m k_b -> 24.24/m
                        # mass of electon / mass of proton
    initialT = 0.01     # Starting temperature
    finalT = parse.(Float64, ARGS[1])     # Final temperature
    numParticles = parse.(Int64, ARGS[2])  # Number of particles    
    numTimeSlices = parse.(Int64, ARGS[3]) # Number of beads along a line
    numEquilibSteps = parse.(Int64, ARGS[4])
    observableSkip = parse.(Int64, ARGS[5])
    numMCSteps = 11*numEquilibSteps + observableSkip 
                        # Number of MC steps to take
    
    numProcesses = parse.(Int64, ARGS[6])  # Number of times to loop/thread/etc.

    if (numProcesses == 1)
        initialT = finalT
    end

    #temp = collect(range(initialT, finalT, length=numProcesses))
    temp = finalT*ones(numProcesses)
    # Imaginary time: \beta/M
    tau = collect(range(initialT, finalT, length=numProcesses)) 
    for index = 1:numProcesses
        #tau[index] = 1/(tau[index] * numTimeSlices)
        # Sets tau to be a constant array
        tau[index] = 1/(numTimeSlices * finalT) 
    end

    binSize = 100       # Data binning

    energyTrace = Float64[]
    potentialTrace = Float64[]
    kineticTrace = Float64[]

    println("Simulation Parameters:")
    println("N\t\t= $(numParticles)")
    println("lambda\t\t= $(lam)")
    println("initialT\t= $(initialT)")
    println("finalT\t\t= $(finalT)")
    println("numMCSteps\t= $(numMCSteps)")

    open("data_T_$finalT-Eq_$numEquilibSteps-Obs_$observableSkip-nB_$numTimeSlices-nP_$numParticles.txt","a") do file
        println(file, numEquilibSteps)
        println(file, observableSkip)
        println(file,finalT)
    end

    for iter=1:numProcesses
        println("Starting run $iter of $numProcesses")

        # Initialize the main data structure
        beads = zeros(Float64,numTimeSlices,numParticles)

        # Random initial positions
        # https://discourse.julialang.org/t/creating-an-array-matrix-with-a-specified-range-of-randomly-generated-numbers/15471
        for tslice = 1:numTimeSlices
            for ptcl = 1:numParticles
                beads[tslice,ptcl] = 0.5 * (-1.0 + 2.0 * rand())
            end
        end

        # Setup the Paths
        determinants = zeros(Float64,numTimeSlices,numParticles)
        potentials = zeros(Float64,numTimeSlices,numParticles)
        expPots = zeros(Float64,numTimeSlices,numParticles)
        energy = [0 0]

        Path = Paths(numParticles,numTimeSlices,beads,lam,tau,determinants,
            potentials,expPots,energy,0)
        
        energyTrace, kineticTrace, potentialTrace = PIMC(numMCSteps,Path,iter,
                                                numEquilibSteps,observableSkip)

        # Copied Python binning from SHO
        deg = temp[iter]
        PyBinData(energyTrace, binSize, numParticles, deg,
                numEquilibSteps,observableSkip,numTimeSlices)

        # make sure the names match like they should
        #open("data_Temp_$finalT-Equil_$numEquilibSteps-Obs_$observableSkip.txt","a") do file
        #    println(file,"Temp = ", temp[iter])
        #end
    end
end

if abspath(PROGRAM_FILE) == @__FILE__
    main()
end
