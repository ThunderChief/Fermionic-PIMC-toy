import sys
#import cProfile

import numpy as np
import math
import time

import multiprocessing  # This should be simplified - just Pool? Or Process?

# ------------------------------------------------------------------------------
# calculate exact energy
def SHOEnergyExact(T):
    ''' The exact SHO energy for \hbar \omega / k_B = 1 '''
    return 0.5/np.tankh(0.5/T)

# ------------------------------------------------------------------------------
# SHO potential energy
def HarmonicOscillator(R):
    ''' SHO potential with m = 1 and \omega = 1 '''
    return 0.5*np.dot(R,R)

# ------------------------------------------------------------------------------
def modTslice(tslice,numSlices):
    return tslice % numSlices

# ------------------------------------------------------------------------------
def cutOff(beadA,beadB,cutoff=10**-12):
    if abs(beadA - beadB) < cutoff:
        return True
    else:
        return False

# ------------------------------------------------------------------------------
# determinant
def determinant(Path,tslice):
    '''In the Lyubartsev paper, J (Lyu.) == M (Prok.), so it corresponds to the 
    number of beads on a line.'''
    '''    tMod = modTslice(tslice,Path.numTimeSlices)
    tModPlus = modTslice(tslice + 1,Path.numTimeSlices)
    tau = Path.tau

    for ptclRow in range(Path.numParticles):
        for ptclCol in range(Path.numParticles):
            # Iterate over the beads. 
            if (cutOff(Path.beads[tMod,ptclRow], Path.beads[tModPlus,ptclCol])):
                Path.expArr[ptclRow,ptclCol] = 0
            else:
                Path.expArr[ptclRow,ptclCol] = np.exp(-1/(2*tau) * 
                    (Path.beads[tMod,ptclRow] - 
                    Path.beads[tModPlus,ptclCol])**2)
    '''    
    return 1#np.linalg.det(Path.expArr)

# ------------------------------------------------------------------------------
# Instantiate the original potentials
def instantiateOriginalPotentials(Path):
    numP = Path.numParticles
    tau = Path.tau
    
    for ptcl in range(Path.numParticles):
        for tslice in range(Path.numTimeSlices):
            tMod = modTslice(tslice,Path.numTimeSlices)
            tModPlus = modTslice(tslice + 1,Path.numTimeSlices)
            vextT = Path.Vext(Path.beads[tMod,ptcl])
            vextTPlus = Path.Vext(Path.beads[tModPlus,ptcl])

            if (cutOff(Path.beads[tMod,ptcl],Path.beads[tModPlus,ptcl])):
                Path.potArr[tslice,ptcl] = 0
            else:
                Path.potArr[tslice,ptcl] = (1/np.math.factorial(numP) * 
                    ((1/(2*math.pi*tau))**(numP/2)) * 
                    np.exp(-tau/2*(vextT + vextTPlus)))

# ------------------------------------------------------------------------------
# Update the potential
def updatePotentials(Path,tslice,ptcl):
    tMod = modTslice(tslice,Path.numTimeSlices)
    tModPlus = modTslice(tslice+1,Path.numTimeSlices)
    numP = Path.numParticles
    tau = Path.tau
    vextT = Path.Vext(Path.beads[tMod,ptcl])
    vextTPlus = Path.Vext(Path.beads[tModPlus,ptcl])

    if (cutOff(Path.beads[tMod,ptcl], Path.beads[tModPlus,ptcl])):
        Path.potArr[tslice,ptcl] = 0
    else:
        Path.potArr[tslice,ptcl] = (1/ np.math.factorial(numP) *
            ((1/(2*math.pi*tau))**(numP/2)) * 
            np.exp(-tau/2*(vextT + vextTPlus)))

# ------------------------------------------------------------------------------
# computeOldAction
def computeOldAction(Path,oldAction,oldBead):
    oldAction = 0.0
    for time in range(Path.numTimeSlices):
        for ptcl in range(Path.numParticles):
            oldBead[time,ptcl] = Path.beads[time,ptcl]
        # Path.PotentialAction already iterates over particles
        oldAction += Path.PotentialAction(time)
    return oldAction

# ------------------------------------------------------------------------------
# computeNewAction
def computeNewAction(Path,RandomTime,newAction):
    newAction = 0.0
    for time in range(Path.numTimeSlices):
        newAction += Path.PotentialAction(time)
    return newAction

# ------------------------------------------------------------------------------
# updateDetPot
def updateDetPot(Path,RandomTime,Particle):
    updatePotentials(Path,RandomTime,Particle)
    Path.detArr[RandomTime-1,Particle] = determinant(Path,RandomTime-1)
    Path.detArr[RandomTime,Particle] = determinant(Path,RandomTime)

# ------------------------------------------------------------------------------
# Paths class
class Paths:
    ''' The set of worldlines, action, and estimators. '''
    def __init__(self,beads,tau,lam):
        self.tau = tau  # Equal to 1/(M*T) = beta/M
        self.lam = lam
        self.beads = np.copy(beads) # This is M (Prok.) == J (Lyu.)
        self.numTimeSlices = len(beads)
        self.numParticles = len(beads[0])
        self.potArr = np.zeros((self.numTimeSlices,self.numParticles))
        self.detArr = np.zeros((self.numTimeSlices,self.numParticles))
        self.expArr = np.zeros((self.numParticles,self.numParticles))

    def SetPotential(self,externalPotentialFunction):
        self.VextHelper = externalPotentialFunction

    def Vext(self,R):
        ''' The external potential energy. '''
        return self.VextHelper(R)

    def Vint(self,alpha,R1,R2):
        ''' The interaction potential energy. '''
        return self.VintHelper(alpha,R1,R2)

    def PotentialAction(self,tslice):
        ''' The potential action '''
        pot = 0.0
        for ptcl in range(self.numParticles):
            pot += self.Vext(self.beads[tslice,ptcl])
        return self.tau*pot

    def KineticEnergy(self):
        '''The thermodynamic kinetic energy estimator.'''
        tot = 0.0
        norm = 1.0/(4.0 * self.lam * self.tau * self.tau)
        for tslice in range(self.numTimeSlices):
            tslicep1 = (tslice + 1) % self.numTimeSlices
            for ptcl in range(self.numParticles):
                delR = self.beads[tslicep1,ptcl] - self.beads[tslice,ptcl]
                tot = tot - norm * np.dot(delR,delR)

        KE = 0.5 * self.numParticles/self.tau + tot/(self.numTimeSlices)
        return KE

    def PotentialEnergy(self):
        '''The operator potential energy estimator.'''
        PE = 0.0
        for tslice in range(self.numTimeSlices):
            for ptcl in range(self.numParticles):
                R = self.beads[tslice,ptcl]
                PE = PE + self.Vext(R)
        return PE/(self.numTimeSlices)

    def Energy(self):
        '''The total energy.'''
        return self.PotentialEnergy() + self.KineticEnergy()

    def Energy2(self,time,ptcl):
        return self.potArr[time,ptcl] * self.detArr[time,ptcl]

# ------------------------------------------------------------------------------
# PIMC function
def PIMC(numSteps,Path):
    '''Perform a path integral Monte Carlo simulation of length numSteps.'''
    pragma = False
    observableSkip = 50
    equilSkip = 1000

    EnergyTrace = []    # Array holding the values of the trace of the density
                        # matrix
    delta = 0.75        # I should ask what this is about
    accepted = 0.0
    shift = 0.0

    numPar = Path.numParticles
    numTime = Path.numTimeSlices
    # Set-up the Potential and Determinant arrays
    
    instantiateOriginalPotentials(Path)
    for tslice in range(numTime):
        for ptcl in range(numPar):
            # Initialize the determinantArray - entries based on original random values
            Path.detArr[tslice,ptcl] = determinant(Path,tslice)
    
    # Iterate
    print("Computing MC simulation...\n")
    for steps in range(numSteps):
        shift = delta * (-1.0 + 2.0 * np.random.random())
        oldBead = np.zeros_like(Path.beads)
            
        # Roll a new position for a single particle
        for Particle in np.random.randint(0,numPar,numPar):
            for RandomTime in np.random.randint(0,numTime,numTime):
                oldAction = 0.0
                newAction = 0.0
                oldAction = computeOldAction(Path,oldAction,oldBead)
                Path.beads[RandomTime,Particle] += shift
                
                # Calculate new potential, determinants due to change
                updateDetPot(Path,RandomTime,Particle)

                # Compute the new action
                newAction = computeNewAction(Path,RandomTime,newAction)

                # Compare the old action and the new action
                # Accept or reject && restore the old bead position

                if np.random.random() < np.exp(-(newAction - oldAction)):
                    accepted += 1
                    if abs(newAction - oldAction) >= 708:   # Checks for overflow
                        print("Overflow error!")
                        print("newAction =", newAction, "oldAction =", oldAction)
                        print("MC Step number =", steps)
                        print("bead positions :", Path.beads)
                        quit()
                else:
                    Path.beads = oldBead
                
                # Measure the energy
                if (steps % observableSkip == 0) and (steps > equilSkip) and (pragma == False):
                    if(steps == 1050):
                        pragma = True
                        print("Now recording energies...\n")
                    EnergyTrace.append(Path.Energy2(RandomTime,Particle))
        
#        with open("bead_positions_distribution_boltz_t1.txt","a") as file1:
            # Write the bead positions to a file
#            for time in range(numTime):
#                print(Path.beads[time,0], file=file1)
        
        # End of the program, basically

    print("Absolute # of accepted: %d" % accepted)
    print("Accepted: %6.4f" % (accepted / numSteps))
    return np.array(EnergyTrace)

# ------------------------------------------------------------------------------
def main():
    lam = 0.5   # \hbar^2/wm k_b
    '''if sys.argv[1] == 'False':
        T = 0.1 # Temperature in Kelvin

        numParticles = 2
        numTimeSlices = 1
        numMCSteps = 101050

    elif sys.argv[1] == 'True':
        T = 1/int(sys.argv[2])

        numParticles = int(sys.argv[3])
        numTimeSlices = int(sys.argv[4])
        numMCSteps = int(sys.argv[5]) + 1050
    else:
        print("Command line input required. The format is \n \
False/True (use defaults/command line arguments) beta numParticles \
numTimeSlices numMCSteps")
        return -1
    '''
    T = 0.01
    numParticles = 1
    numTimeSlices = 20
    numMCSteps = 101050

    numProcesses = 100
    targetTemp = 1.0
    tau = np.linspace(T,targetTemp,numProcesses)/numTimeSlices 
        # Imaginary time - beta/numTimeSlices
    binSize = 100       # Data binning

    reportedEnergies = np.zeros(numProcesses)
    temp = np.linspace(T,targetTemp,numProcesses)

    print('Simulation Parameters:')
    print('N      = %d' % numParticles)
#    print('tau    = %6.4f' % tau)
    print('lambda = %6.4f' % lam)
    print('T      = %4.2f' % T)
    print('targetTemp = %4.2f' % targetTemp)
    print('numMCSteps = ', numMCSteps)
    # fix the random seed for repeatability
    #np.random.seed(1173)

    for i in range(0,numProcesses):
        # initialize the main data structure
        beads = np.zeros([numTimeSlices,numParticles])

        # random initial positions (classical state)
        for tslice in range(numTimeSlices):
            for ptcl in range(numParticles):
                beads[tslice,ptcl] = 0.5 * (-1 + 2.0 * np.random.random())

        # setup the paths
        Path = Paths(beads,tau[i],lam)
        Path.SetPotential(HarmonicOscillator)

        # Compute the energy via path-integral Monte Carlo

        Energy = PIMC(numMCSteps,Path)

#        with open("energy.txt","w") as file2:
#            for x in range(len(Energy)):
#                print(Energy[x], file=file2)

        # Do some simple binning statistics
        numBins = int(1.0*len(Energy)/binSize)
        print(f"numBins = {numBins}")
        print(f"len(Energy) = {len(Energy)}")
        slices = np.linspace(0, len(Energy),numBins+1,dtype=int)
        print(f"Slices: {slices}")
        binnedEnergy = np.add.reduceat(Energy, slices[:-1]) / np.diff(slices)
        reportedEnergies[i] = np.mean(binnedEnergy)
        print(f"Reported Energy for temperature {temp[i]}: {reportedEnergies[i]}")

    # Output the final result
    with open("energy_v_temp.txt","w") as file2:
        for x in range(len(reportedEnergies)):
            print(temp[x], file=file2)
            print(Energy[x], file=file2)

    print('Energy = %8.4f +/- %6.4f' % (np.mean(binnedEnergy),
                                    (np.std(binnedEnergy)/np.sqrt(numBins-1))))
    print('Eexact = %8.4f' % (float(numParticles) * SHOEnergyExact(T)))

# ------------------------------------------------------------------------------
# main function loop 
if __name__ == "__main__":
    time1 = time.time()
    main()
    time2 = time.time()
    print("Time elapsed: %6.4f seconds" % (time2 - time1))