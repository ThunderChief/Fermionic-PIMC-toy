#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string>   // Command-line argument & file name handling

#include "1.0-sho.h"

struct Paths
{
    int numParticles;
    int numTimeSlices;
    double **beads;
    double lam;
    double *tau;
    double **determinants;  // Shape is (numTimeSlices, numParticles)
    double **potentials;
    double **expPots;
    double energy[2] = { 0 };
    int numAccepted = 0;
};

inline double ExtPotential(double R)
{
    return 1/2*R*R;
}

inline int ModTslice(int tSlice, int numSlices)
{
    return (( tSlice - 1) % numSlices) + 1;
}

inline bool CutOff(double beadA, double beadB, double cutoff=pow(10.0,-12))
{
    return (abs(beadA - beadB) < cutoff);
}

double PotentialAction(double beadVector[], double tau, int numParticles)
{
    int i = 0;
    double pot = 0;

    do
    {
        pot += ExtPotential(beadVector[i]);
        
        i++;
    } while(i < numParticles);
    
    return tau * pot;
}

double KineticEnergy(Paths &Path, double tau)
//double KineticEnergy(double Path.beads[][], double lam, double tau, 
//        int numTimeSlices, int numParticles, double kinEn)
{
    int i = 0;
    int j = 0;
    double tot = 0.0;
    double norm = 1/(4 * Path.lam * tau * tau);
    double delR = 0;

    do
    {
        int tModPlus = ModTslice(i + 1, Path.numTimeSlices);
        do
        {
            delR = Path.beads[tModPlus][j] - Path.beads[i][j];

            tot = tot - norm * delR * delR;
            j++;
        } while(j < Path.numParticles);

        i++;
    } while(i < Path.numTimeSlices);
    // kinEn = 1/2 * numParticles/tau + tot/numTimeSlices;
    return 1/2 * Path.numParticles/tau + tot/Path.numTimeSlices;
}

double PotentialEnergy(Paths &Path)
// double PotentialEnergy(double Path.beads[][], int numTimeSlices, int numparticles,
//        double potEn)
{
    int i = 0;
    int j = 0;
    double tot = 0;
    double R = 0;
  
    do
    {
        do
        {
            R = Path.beads[i][j];
            tot += ExtPotential(R);
            
            j++;
        } while (j < Path.numParticles);

        i++;
    } while(i < Path.numTimeSlices);

    // potEn = tot/numTimeSlices;
    return tot/Path.numTimeSlices;
}

// double Energy(double Path.beads[][], double lam, double tau, int numTimeSlices,
//        int numParticles, double kinEn, double potEn)
double Energy(Paths &Path, double tau)
{
    double kinetic = 0;
    double potential = 0;

//    kinetic = KineticEnergy(beads, lam, tau, numTimeSlices, numParticles, kinEn);
//    potential = PotentialEnergy(beads, numTimeSlices, numParticles, potEn);
    kinetic = KineticEnergy(Path, tau);
    potential = PotentialEnergy(Path);

    return kinetic + potential;
}

double Determinant(Paths &Path, int tSlice)
{
    return 1;
    // Short-circuiting this thing for both dev. time and for Boltzmannons
    
    int tMod = ModTslice(tSlice, Path.numTimeSlices);
}

void InstantiatePotentials(Paths &Path, int iter)
{
    int i = 0;
    int j = 0;
    int tMod = 0;
    int tModPlus = 0;
    double vextT = 0;
    double vextTplus = 0;

    do
    {
        do
        {
            tMod = ModTslice(i, Path.numTimeSlices);
            tModPlus = ModTslice(i + 1, Path.numTimeSlices);
            vextT = ExtPotential(Path.beads[tMod][j]);
            vextTplus = ExtPotential(Path.beads[tModPlus][j]);

            CutOff(Path.beads[tMod][j], Path.beads[tModPlus][j]) == true ?
                Path.potentials[i][j] = 0 : 
                Path.potentials[i][j] = -1/2 * (vextT + vextTplus);

            j++;
        } while (j < Path.numParticles);

        i++;
    } while(i < Path.numTimeSlices);
}

double ComputeAction(Paths &Path, int tSlice, int iter)
{
    int i = 0;
    double action = 0;
    int tMod = ModTslice(tSlice, Path.numTimeSlices);
    int tModPlus = ModTslice(tSlice + 1, Path.numTimeSlices);
    double vextT = 0;
    double vextTplus = 0;

    do
    {
        vextT = ExtPotential(Path.beads[tMod][i]);
        vextTplus = ExtPotential(Path.beads[tModPlus][i]);

        CutOff(Path.beads[tMod][i], Path.beads[tModPlus][i]) == true ?
                action += 0 : action += 1/2 * (vextT + vextTplus);

        i++;
    } while(i < Path.numParticles);

    return action;
}

void PIMC(Paths &Path, int numSteps, int iter, int numEquilStep, int observSkip)
{
    double energyTrace[] = { 0 };
    double potentialTrace[] = { 0 };
    double kineticTrace [] = { 0 };
    double delta = 1.0;

    int i = 0;
    int j = 0;
    // Initialize the determinants array
}

void BinData(double *energyTrace, int binSize, int numParticles, double temp,
        int equil, int obs, int numTimeSlices)
{
    // Does the data binning part of the algorithm
}

int main(int argc, char *argv[])
{
    std::stringstream tempConvert{ argv[1] };
    std::stringstream intArg2{ argv[2] };
    std::stringstream intArg3{ argv[3] };
    std::stringstream intArg4{ argv[4] };
    std::stringstream intArg5{ argv[5] };
    std::stringstream intArg6{ argv[6] };

    int multiple = 11;              // Arbitrary, random, just because

    double lam = 24.24 * 1836;       // 1836 = m_p/m_e; not sure about 24.24
    double initialT = 0.01;
    double finalT << tempConvert;    // Final temperature
    int numParticles << intArg2;    // Number of particles
    int numTimeSlices << intArg3;   // Number of time slices/beads on a line
    int numEquilibSteps << intArg4; // Number of steps to equilibriate
    int observableSkip << intArg5;  // Number of steps to skip per observation
    int numMCsteps = multiple*numEquilibSteps + observableSkip;
    int numProcesses << intArg6;    // Number of times to loop

    if (numProcesses == 1)
        intialT = finalT;

    double temp[numProcesses] = { 0 };
    double tau[numProcesses] = { 0 };
    int i = 0;
    int j = 0;
    while(i < numProcesses)
    {
        temp[i] = 1;
        tau[i] = 1/(numTimeSlices * finalT);
        i++;
    }
    
    int binSize = 100;

    int arrayLength = ((multiple-1)*numEquilibSteps)/observableSkip + 1;
    double energyTrace[arrayLength] = { 0 };
    double potentialTrace[arrayLength] = { 0 };
    double kineticTrace[arrayLength] = { 0 };

    printf("Simulation Parameters:a\n");
    printf("N\t\t= %d", numParticles);
    printf("lambda\t\t= %f", lam);
    printf("initialT\t= %f", initialT);
    printf("finalT\t= %f", finalT);
    printf("numMCsteps\t- %f", numMCsteps);

    // File management here
    std::string name = "data_T_" + std::to_string(finalT) + "-Eq_" + 
        std::to_string(numEquilibSteps) + "-Obs_" + 
        std::to_string(observableSkip) + "-nB_" + to_string(numTimeSlices) + 
        "nP_" + to_string(numParticles) + ".txt";
    for(int k = 0; k < numProcesses; k++)
    {
        print("Starting run %d of %d\n", k, numProcesses);
    
    return 0;
}
