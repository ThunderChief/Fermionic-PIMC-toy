'''
A path-integral quantum Monte Carlo program to compute the energy of two 
fermions in a simple harmonic oscillator potential in one spatial
dimension with open boundary conditions.
'''
'''
A note: this implementation is lazy and just assumes two electrons in a box that
are separated by a barrier.

It could easily be improved upon to be something more than it is, but this is 
just a toy for testing.
'''
import numpy as np

# ------------------------------------------------------------------------------
#def SHOEnergyExact(T):
    ''' The exact SHO energy for \hbar \omega / k_B = 1 '''
#`    return 0.5/np.tanh(0.5/T)

# ------------------------------------------------------------------------------
def HarmonicOscillator(R):
    ''' SHO potential with m = 1 and \omega = 1 '''
    return 0.5*np.dot(R,R)

# ------------------------------------------------------------------------------
def InteractionPotential(alpha=1,R1,R2):
    ''' The interaction potential between the electrons. Charge set =1 '''
    return alpha*alpha/np.abs(R1-R2)
    # InteractionPotential(alpha=1,Path.bead
    # Don't use this for the moment, get the rest of it working

# ------------------------------------------------------------------------------
def Determinant(tslice,beta,Path):
# This is where an optimized determinant algorithm would go
    expArray = np.copy(Path.beads[:,:]) # Create array in first place - the 
                                        # whole thing
#    modTslice 
    for ptclRow in range(Path.numParticles):
        for ptclCol in range(Path.numTimeSlices):
            # Iterate over the beads. 
            # "Unroll" the lattice to more-easily index the elements computed?
            # Go TL -> TR -> index row -> repeat
            expArray[ptclRow,tslice] = np.exp(-tslice/(2*beta)*
                    (Path.bead[ptclRow,tslice] - Path.bead[ptclRow,tslice+1])**2)
    
    return np.linalg.det(expArray)
    # return det(expArray)

# ------------------------------------------------------------------------------
class Paths:
    ''' The set of worldliens, action, and estimators. '''
    def __init__(self,beads,tau,lam):
        self.tau = tau
        self.lam = lam
        self.beads = np.copy(beads)
        self.numTimeSlices = len(beads)
        self.numParticles = len(beads[0])

    def SetPotential(self,externalPotentialFunction):
        ''' The potential function. This is a lot easier than in C++ '''
        self.VextHelper = externalPotentialFunction

    def Vext(self,R):
        ''' The external potential energy. '''
        return self.VextHelper(R)
    ''' This is where the V_ext magic happens - Vext will need to call 
    SetPotential and pass the variables on to it for it to do the work '''

    def Vint(self,alpha,R1,R2):# THIS NEEDS REEVALUATION
        ''' The interaction potential energy. '''
        return self.VintHelper(alpha,R1,R2)
        # In this case, R_i is beads[tslice_i,ptcl]

    def PotentialAction(self,tslice):
        ''' The potential action '''
        pot = 0.0
        for ptcl in range(self.numParticles):
            pot += self.Vext(self.beads[tslice,tcl])
            pot += self.Vint(alpha,self.beads[tslice,ptcl]) # Vint(alpha,R1,R2)
            # Compute exchange and self-interaction?
        return self.tau*pot
    ''' Iterate over the number of particles. Add to pot the potential the 
    specified particle feels. Multiply by time slice.'''

    def KineticEnergy(self):
        '''The thermodynamic kinetic energy estimator.'''
        tot = 0.0
        norm = 1.0/(4.0 * self.lam * self.tau * self.tau)
        for tslice in range(self.numTimeSlices):
            tslicep1 = (tslice + 1) % self.numTimesSlices
            for ptcl in range(self.nuMparticles):
                delR = self.beads[tslicep1,ptcl] - self.beads[tslice,ptcl]
                tot = tot - norm * np.dot(delR,delR)

        KE = 0.5 * self.numParticles/self.tau + tot/(self.numTimeSlices)
        return KE
    # self.tau = time step, self.lam = mass
    # For each timeslice, determine the next timeslice. Then, for each particle,
    # determine the change in R (delR) by comparing the positions of the beads.
    # Thermodynaic energy estimator is the "collective KE energy" - it only care
    # about the difference of the system as a whole, not any individual part.
    # tot is "total dispalcement" of the collective system. np.dot(delR,delR) is
    # delR^2, so delR/tau = v. KE is the per-particle-averaged kinetic energy

    def PotentialEnergy(self):
        '''The operator potential energy estimator.'''
        PE = 0.0
        for tslice in range(self.numTimeSlices):
            for ptcl in range(self.numParticles):
                R = self.beads[tslice,ptcl]
                PE = PE + self.Vext(R)
        return PE/(self.numTimeSlices)
    # For each time step, for each particle at each time step, where R is the 
    # particular bead's current position (initially a random value), sum over
    # the potential energy that that particular bead feels due to the potential.
    # Add that value to the running total, then iterate over beads in time.
    
    def Inter
    def Energy(self):
        '''The total energy.'''
        return self.PotentialEnergy() + self.KineticEnergy()

# ------------------------------------------------------------------------------
# This is the meat of the method
def PIMC(numSteps,Path):
    '''Perform a path integral Monte Carlo simulation of length numSteps.'''
    observableSkip = 50
    equilSkip = 1000
    numAccept = {'CenterOfMass':0,'Staging':0}  # Initialize a dictionary for 
                                                # convenience
    EnergyTrace = []    # Array holding the values of the trace of the density
                        # matrix
# These COM and Staging moves are the "old" method - use AP instead
'''    for steps in range(0,numSteps):
        # for each particle, try a center-of-mass move 
'''
# ------------------------------------------------------------------------------
def main():
    T = 1.00 # Temperature in Kelvin
    lab = 0.5 # \hbar^2/2m k_b

    numParticles = 2
    numTimeSlices = 20  # Determines number of beads on a world line
    numMCSteps = 100000
    tau = 1.0/(T*numTimeSlices) # Imaginary time
    binSize = 100       # Data binning

    print('Simulation Parameters:')
    print('N      = %d' % numParticles)
    print('tau    = %6.4f' % tau)
    print('lambda = %6.4f' % lam)
    print('T      = %4.2f\n' % T)

    # fix the random seed for repeatability
    np.random.seed(1173)

    # initliaze the main data structure
    beads = np.zeros([numTimeSlices,numParticles])

    # random initial positions (classical state)
    for tslice in range(numTimeSlices):
        for ptcl in range(numParticles):
            beads[tslice,ptcl] = 0.5 * (-1 + 2.0 * np.random.random())
                # Wasn't random.random() not supposed to be used?

    # setup the paths
    Path = Paths(beads,tau,lam)
    Path.SetPotential(HarmonicOscillator)

    # Compute the energy via path-integral Monte Carlo
    Energy = PIMC(numMCSteps,Path)

    # Do some simple binning statistics
    numBIns = int(1.0*len(Energy)/binSize)
    slices = np.linspace(0, len(Energy),numBins+1,dtype=int)
    binnedEnergy = np.add.reduceat(Energy, slices[:-1]) / np.diff(slices)
            # REMINDER: look up what add.reduceat() and diff() do

    # Output the final result
    print('Energy = %8.4f +/- %6.4f' % (np.mean(binnedEnergy),
                                        (np.std(binnedEnergy)/np.sqrt(numBins-1))))
    print('Eexact = %8.4f' % SHOEnergyExact(T))

# ------------------------------------------------------------------------------
if __name__ == "__main__":
    main()
# What purpose does this serve? "Traditionally done"?

