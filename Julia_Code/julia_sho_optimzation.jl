using Test              # Get the testing going
using InteractiveUtils  # Optimization testing
include("./3-julia_sho.jl")

function TestMain()
# =============================================================================
# Initialization and instantiation
# =============================================================================
    testSections = parse.(Int64, ARGS[1])
    lam = 24.24*1836
    initialT = 0.01
    finalT = 1.00
    numParticles = 1
    numTimeSlices = 20
    numEquilibSteps = 5000
    observableSkip = 100
    numMCSteps = 11*numEquilibSteps + observableSkip
    numProcesses = 10
    temp = finalT*ones(numProcesses)
    tau = collect(range(initialT, finalT, length=numProcesses))
    for index = 1:numProcesses
        tau[index] = 1/(numTimeSlices * finalT)
    end
    binSize = 100
    energyTrace = Float64[]
    potentialTrace = Float64[]
    kineticTrace = Float64[]
    beads = zeros(Float64, numTimeSlices, numParticles)
    for tslice = 1:numTimeSlices
        for ptcl = 1:numParticles
            beads[tslice,ptcl] = 0.5 * (-1.0 + 2.0*rand())
        end
    end
    
    iter = 2
    tslice = 1
    println("tslice = ", tslice)
    println("iter = ", iter)
# =============================================================================
# Testing of simple functions
# =============================================================================
    if (testSections == 1) || (testSections == 0)
        println(beads)
        print("\n")
        println("Optimizations for cutOff(): ")
        
        @code_warntype(cutOff(1.21,1.20))
        println()
        println()
        println("Optimizations for ModTslice(tslice,20): ")
        
        @code_warntype ModTslice(tslice,20)
        println("\n")

        println("Testing ExtPotential(3): ")
        @code_warntype ExtPotential(3.0)
        println("\n")
        if testSections == 1
            exit()
        end
    end
# =============================================================================
# Testing of complicated functions
# =============================================================================
    if (testSections == 2) || (testSections == 0)
        testBeads = ones(Float64,numTimeSlices,numParticles)
        determinants = zeros(Float64,numTimeSlices,numParticles)
        potentials = zeros(Float64,numTimeSlices,numParticles)
        expPots = zeros(Float64,numTimeSlices,numParticles)
        energy = [0 0]
        Path = Paths(numParticles,numTimeSlices,testBeads,lam,tau,determinants,
                     potentials,expPots,energy,0)
        println("Path.tau[iter=2] = ", Path.tau[iter])

        # PotentialAction
        println("Optimizations for PotentialAction(Path,tslice,2): ")
                @code_warntype PotentialAction(Path,tslice,iter)
        println("\n")

        # KineticEnergy
        println("Optimizations for KineticEnergy(Path,iter): ")
                @code_warntype KineticEnergy(Path,iter)
        println("\n")
        
        # PotentialEnergy
        println("Optimizations for PotentialEnergy(Path,iter): ")
                @code_warntype PotentialEnergy(Path,iter)
        println("\n")

        # Energy
        println("Testing Energy(Path,iter): ", @test Energy(Path,iter) == 10.5)

        # Determinant
        println("Testing Determinant(Path,tslice,iter): ", 
                @test Determinant(Path,tslice,iter) == 1)

        # InstantiatePotentials
        InstantiatePotentials(Path,iter)
        passed = false
        for tslice = 1:Path.numTimeSlices
            if (Path.potentials[tslice,1] != 0)
                @test (passed == true)
                println("Testing InstantiatePotentials(Path,iter): ", 
                        @test passed)
                break
            end
        end
        println("Testing InstantiatePotentials(Path,iter): ", 
                @test (passed == false))

        # ComputeAction
        #println("Testing ComputeAction(Path,tslice,iter): ",
        #        @test ComputeAction(Path,tslice,iter) == 

        if testSections == 2
            exit()
        end
    end
end

if abspath(PROGRAM_FILE) == @__FILE__
    TestMain()
end
